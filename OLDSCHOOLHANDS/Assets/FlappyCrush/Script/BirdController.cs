﻿using UnityEngine;
using System.Collections;

public class BirdController : MonoBehaviour {
	[Tooltip("drop Brid object child in here")]
	public Transform Bird;
	public float minSpeed = 5f;
	public float maxSpeed = 10f;

	public AudioClip hitSound;
	public AudioClip deadSound;
	[Tooltip("spawn blood effect when bird dead")]
	public GameObject deadFX;

	private float speed;
	private bool hitPipe = false;
	private float x;
	private float y;
	private Animator anim;
	// Use this for initialization
	void Start () {
		speed = Random.Range (minSpeed, maxSpeed);	//get random speed
		anim = GetComponent<Animator>();
	}

	void Update(){
		if (GameManager.CurrentState == GameManager.GameState.Fail) {	//if one bird escape, all of them escape too
			anim.SetTrigger ("Escape");
			GetComponent<Rigidbody2D> ().isKinematic = true;
			transform.position = Vector3.MoveTowards (transform.position, new Vector3 (10,3, transform.position.z), 0.1f);
		} else {
			if (!hitPipe) {	
				transform.Translate (Vector3.right * speed * Time.deltaTime);	//moving
			} else {		//if hit the pipe's side, the bird will stand back a random distance and keep moving forward
				transform.position = Vector3.MoveTowards (transform.position, new Vector3 (x, y, transform.position.z), 0.5f);
				if (Mathf.Abs (transform.position.x - x) < 0.2f)
					hitPipe = false;	//finish stand back. keep moving
			}
		}
	}

	//action sent by PipeController script
	public void Damage(){
		GameManager.Score++;	//add 1 score
		SoundManager.PlaySfx (deadSound);
		if (GlobalValue.isBlood)
			Instantiate (deadFX, transform.position, Quaternion.identity);	//spawn blood fx
		BoneManager.instance.SpawnBone ();	//call action spawn bone
		Destroy (gameObject);	//destroy this bird
	}

	//detect bird colllide with the pipe
	void OnTriggerEnter2D (Collider2D other)
	{
		if (GameManager.CurrentState != GameManager.GameState.Playing)
			return;		//no detect with anything when not in Playing mode
		
		if (other.gameObject.CompareTag ("PipeSide")) {
			SoundManager.PlaySfx (hitSound, 0.5f);

			x = transform.position.x - Random.Range (1.5f, 3.5f);
			if (transform.position.y >= 0)
				y = transform.position.y - 1f;
			else
				y = transform.position.y + 1f;
			
			hitPipe = true;
		} else if (other.gameObject.CompareTag ("Escape")) {
			GameManager.instance.Fail ();
		}

	}
}
