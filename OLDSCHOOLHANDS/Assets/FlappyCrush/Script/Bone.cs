﻿using UnityEngine;
using System.Collections;

public class Bone : MonoBehaviour {
	public float minForce = 200;
	public float maxForce = 500f;
	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody2D> ().AddForceAtPosition (new Vector2(Random.Range (minForce, maxForce),200f),new Vector2(transform.position.x,transform.position.y+0.1f));
		StartCoroutine (WaitForDisable (1.5f));
	}
	
	IEnumerator WaitForDisable(float time){
		yield return new WaitForSeconds (time);
		GetComponent<Rigidbody2D> ().isKinematic = true;
		enabled = false;
	}
}
