﻿using UnityEngine;
using System.Collections;

public class PipeController : MonoBehaviour {
	[Tooltip("sound when the pipes shot down")]
	public AudioClip pipeSound;


	private Animator anim;
	private BoxCollider2D boxColl;
	private int combo = 0;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		boxColl = GetComponent<BoxCollider2D> ();
		boxColl.enabled = false;	//not allow detect bird when the pipes not shut down yet
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0) && GameManager.CurrentState != GameManager.GameState.Fail)
			anim.SetTrigger ("Shut");	//set trigger action for animator to shut down the pipes
	}

	//send by event animation
	public void Shut(){
		SoundManager.PlaySfx (pipeSound);
		boxColl.enabled = true;
		StartCoroutine (WaitTurnOffBoxCollider (0.1f));	//wait 0.1 second and disable detect bird when the pipes are open
	}

	void OnTriggerEnter2D(Collider2D other) {
		combo++;
		other.gameObject.transform.parent.SendMessage ("Damage", SendMessageOptions.DontRequireReceiver);

	}

	IEnumerator WaitTurnOffBoxCollider(float time){
		yield return new WaitForSeconds (time);
		boxColl.enabled = false;
		UIManager.instance.Combo (combo);
		combo = 0;
	}
}
