﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	public static GameManager instance;

	public enum GameState{Menu,Fail,Pause,Playing};
	private GameState state;
	private int score = 0;

	public static GameState CurrentState {
		get{ return instance.state; }
		set{ instance.state = value; }
	}

	public static int Score{
		get{ return instance.score; }
		set{ instance.score = value; }
	}

	public static int Best{
		get{ return PlayerPrefs.GetInt("best",0); }
		set{ PlayerPrefs.SetInt ("best", value); }
	}

	public AudioClip soundFail;
	public AudioClip soundFlying;

	// Use this for initialization
	void Start () {
		instance = this;
		state = GameState.Menu;
		AdsCotroller.HideAds ();	//hide ads when playing
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Escape) && state != GameState.Playing)
			Application.Quit ();
	}

	public void Fail(){
		if (state == GameState.Playing) {
			state = GameState.Fail;

			SoundManager.PlaySfx (soundFail);
			SoundManager.PlaySfx (soundFlying);

			UIManager.instance.ShowGameOver ();
			AdsCotroller.ShowAds ();
		}
	}
}
