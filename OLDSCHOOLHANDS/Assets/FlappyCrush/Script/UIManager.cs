﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
/*
 * This script control all GUI in game
 * Call its function with UIManager.action
*/
public class UIManager : MonoBehaviour {
	public GameObject Menu;
	public GameObject UI;
	public GameObject GameOver;
	public GameObject GamePause;



	public AudioClip clickSound;
	public AudioClip comboSound;

	public GameObject StartBut;
	public Text uiScore;
	public Text goScore;
	public Text goBest;
	[Header("Medal")]
	public Image medal;
	public int scoreBronze = 10;
	public int scoreSilver = 100;
	public int scoreGold = 500;
	public Sprite medalBronze;
	public Sprite medalSilver;
	public Sprite medalGold;
	public Text combo;
	[Header("Sound Button")]
	public Image soundButtonUI;
	public Image soundButtonGO;
	public Sprite soundOn;
	public Sprite soundOff;
	[Header("Blood Button")]
	public Image BloodButtonUI;
	public Image BloodButtonGO;
	public Sprite BloodOn;
	public Sprite BloodOff;



	public static UIManager instance;

	private Animator comboAnim;

	// Use this for initialization
	void Start () {
		instance = this;
		comboAnim = combo.GetComponent<Animator> ();

		//init UI
		Menu.SetActive (true);
		UI.SetActive (false);
		GameOver.SetActive (false);
		GamePause.SetActive (false);

		//init buttons Sound and Blood
		InitButtons();

		if (GlobalValue.isRestart)
			Play ();
	}

	void Update(){
		uiScore.text = GameManager.Score + "";	//update score text
	}

	//Play button in Menu Ui
	public void Play(){
		Menu.SetActive (false);
		UI.SetActive (true);
	}

	//StartBut button
	public void PlayGame(){
		GameManager.CurrentState = GameManager.GameState.Playing;
		StartBut.SetActive (false);
	}

	//Restart Button
	public void Restart(){
		SoundManager.PlaySfx (clickSound);
		GlobalValue.isRestart = true;
//		Application.LoadLevel (Application.loadedLevel);
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	//Pause and Resume Button
	public void Pause(){
		SoundManager.PlaySfx (clickSound);
		if (Time.timeScale == 1) {
			UI.SetActive (false);
			GamePause.SetActive (true);
			Time.timeScale = 0;
		} else {
			UI.SetActive (true);
			GamePause.SetActive (false);
			Time.timeScale = 1;
		}
	}


	public void ShowGameOver(){
		StartCoroutine (ShowGameOverDelay (1.5f));	//Show GameOver menu after delay time
	}

	//called by PipeController
	public void Combo(int no){	
		if (no >= 2) {	//just show up combo when hit over 2 birds in one time
			GameManager.Score += no;
			combo.text = "COMBO X" + no;
			comboAnim.SetTrigger ("combo");
			SoundManager.PlaySfx (comboSound);


		}
	}

	//Sound buttons
	public void Sound(){
		if (GlobalValue.isSound) {
			GlobalValue.isSound = false;
			SoundManager.SoundVolume = 0;
			soundButtonUI.sprite = soundOff;
			soundButtonGO.sprite = soundOff;
		} else {
			GlobalValue.isSound = true;
			SoundManager.SoundVolume = 1;
			soundButtonUI.sprite = soundOn;
			soundButtonGO.sprite = soundOn;
		}
		SoundManager.PlaySfx (clickSound);
	}

	//Blood buttons
	public void Blood(){
		SoundManager.PlaySfx (clickSound);
		if (GlobalValue.isBlood) {
			GlobalValue.isBlood = false;
			BloodButtonUI.sprite = BloodOff;
			BloodButtonGO.sprite = BloodOff;
		} else {
			GlobalValue.isBlood = true;
			BloodButtonUI.sprite = BloodOn;
			BloodButtonGO.sprite = BloodOn;
		}
	}

	//GEt status of those button when restart game
	private void InitButtons(){
		//init Sound buttons
		if (GlobalValue.isSound) {
			soundButtonUI.sprite = soundOn;
			soundButtonGO.sprite = soundOn;
		} else {
			soundButtonUI.sprite = soundOff;
			soundButtonGO.sprite = soundOff;
		}
		//init Blood buttons
		if (GlobalValue.isBlood) {
			BloodButtonUI.sprite = BloodOn;
			BloodButtonGO.sprite = BloodOn;
		} else {
			BloodButtonUI.sprite = BloodOff;
			BloodButtonGO.sprite = BloodOff;
		}
	}

	IEnumerator ShowGameOverDelay(float time){
		yield return new WaitForSeconds (time);
		UI.SetActive (false);
		GameOver.SetActive (true);
		if (GameManager.Score > GameManager.Best) {
			GameManager.Best = GameManager.Score;
		}
		goScore.text = GameManager.Score+"";
		goBest.text = GameManager.Best+"";

		//set medal
		if (GameManager.Score > scoreGold)
			medal.sprite = medalGold;
		else if (GameManager.Score >= scoreSilver)
			medal.sprite = medalSilver;
		else if (GameManager.Score >= scoreBronze)
			medal.sprite = medalBronze;
	}
}
