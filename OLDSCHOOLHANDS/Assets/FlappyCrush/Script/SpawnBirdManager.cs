﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnBirdManager : MonoBehaviour {
	public Transform limitedUp;
	public Transform limitedDown;
	public GameObject[] ListBirds;
	public AudioClip soundSpawn;

	[Header("Level 1")]
	[Header("Level difficult")]

	public float minDelaySpawn = 0.5f;
	public float maxDelaySpawn = 1f;
	[Header("Level 2")]
	public int scoreLevel2 = 30;
	public float minDelaySpawn2 = 0.4f;
	public float maxDelaySpawn2 = 0.9f;
	[Header("Level 3")]
	public int scoreLevel3 = 60;
	public float minDelaySpawn3 = 0.3f;
	public float maxDelaySpawn3 = 0.8f;

	private float minDelay;
	private float maxDelay;


	// Use this for initialization
	void Start () {
		minDelay = minDelaySpawn;
		maxDelay = maxDelaySpawn;

		StartCoroutine (SpawnBird (Random.Range (minDelay, maxDelay)));

	}

	void Update(){
		if (GameManager.Score >= scoreLevel3) {
			minDelay = minDelaySpawn3;
			maxDelay = maxDelaySpawn3;
		} else if (GameManager.Score >= scoreLevel2) {
			minDelay = minDelaySpawn2;
			maxDelay = maxDelaySpawn2;
		}
	}

	IEnumerator SpawnBird(float time){
		yield return new WaitForSeconds (time);
		if (GameManager.CurrentState == GameManager.GameState.Playing) {
			float randomY = Random.Range (limitedDown.position.y, limitedUp.position.y);
			Instantiate (ListBirds [Random.Range (0, ListBirds.Length)], new Vector3 (transform.position.x, transform.position.y + randomY, transform.position.z), Quaternion.identity);
			SoundManager.PlaySfx (soundSpawn);
		} else if (GameManager.CurrentState == GameManager.GameState.Fail) {
			enabled=false;	//end script when gameover
		}
		StartCoroutine (SpawnBird (Random.Range (minDelay, maxDelay)));
	}
}